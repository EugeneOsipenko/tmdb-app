package com.eugeneosipenko.tmdb.util

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.eugeneosipenko.tmdb.App
import com.eugeneosipenko.tmdb.di.CoreComponent

inline fun <reified T : ViewModel> AppCompatActivity.injectViewModel(factory: ViewModelProvider.Factory): T {
    return ViewModelProviders.of(this, factory)[T::class.java]
}

fun AppCompatActivity.component(): CoreComponent {
    return (application as App).component
}