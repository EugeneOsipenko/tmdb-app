package com.eugeneosipenko.tmdb.data

import java.text.SimpleDateFormat
import java.util.*

data class Range(val startDate: String) {
    companion object {

        private val DATE_FORMAT = SimpleDateFormat("yyyy-MM-dd", Locale.US)

        fun today() = Range(
            DATE_FORMAT.format(Date())
        )

        fun forLastDays(days: Int): Range {
            val calendar = Calendar.getInstance()
            calendar.add(Calendar.DAY_OF_MONTH, -days)
            return Range(
                DATE_FORMAT.format(
                    calendar.time
                )
            )
        }
    }
}