package com.eugeneosipenko.tmdb.data

import androidx.paging.PositionalDataSource
import com.eugeneosipenko.tmdb.db.MovieChangesDAO
import com.eugeneosipenko.tmdb.db.MovieDAO
import com.eugeneosipenko.tmdb.db.MovieModel
import com.eugeneosipenko.tmdb.network.MovieService
import com.eugeneosipenko.tmdb.network.model.MovieEntity
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable

class MovieDataSource(
    private val service: MovieService,
    private val movieChangesDAO: MovieChangesDAO,
    private val movieDAO: MovieDAO,
    invalidate: Observable<Boolean>
) : PositionalDataSource<MovieModel>() {

    private val disposable = CompositeDisposable()

    init {
        disposable.add(invalidate.subscribe {
            invalidate()
            disposable.clear()
        })
    }

    override fun loadRange(params: LoadRangeParams, callback: LoadRangeCallback<MovieModel>) {
        disposable.add(buildLoadObservable(params.startPosition, params.loadSize)
            .subscribe { data, error ->
                callback.onResult(data)
            })
    }

    override fun loadInitial(params: LoadInitialParams, callback: LoadInitialCallback<MovieModel>) {
        disposable.add(buildLoadObservable(params.requestedStartPosition, params.requestedLoadSize)
            .subscribe { data, error ->
                callback.onResult(data, 0)
            })
    }

    private fun convert(entity: MovieEntity): MovieModel {
        return MovieModel(
            entity.id,
            entity.title,
            entity.genres.map { it.name },
            entity.overview,
            entity.backdropPath,
            entity.posterPath,
            entity.releaseDate,
            entity.voteAverage
        )
    }

    private fun buildLoadObservable(offset: Int, count: Int) =
        movieChangesDAO.getChanges(offset, count)
            .toObservable()
            .flatMapIterable { it }
            .flatMap { change ->
                movieDAO.findById(change.id)
                    .switchIfEmpty(
                        service.getDetails(change.id)
                            .singleElement()
                            .map { convert(it) }
                            .map { store(it) }
                            .onErrorReturn { emptyModel() })
                    .toObservable()
            }
            .toList()

    private fun store(movie: MovieModel): MovieModel {
        movieDAO.insert(movie)
        return movie
    }

    private fun emptyModel() =
        MovieModel(
            -1, "Error", arrayListOf(), "",
            "", "", "", 0f
        )
}