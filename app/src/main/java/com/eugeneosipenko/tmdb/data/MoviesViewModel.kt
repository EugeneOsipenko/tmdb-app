package com.eugeneosipenko.tmdb.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.eugeneosipenko.tmdb.db.MovieChangesDAO
import com.eugeneosipenko.tmdb.db.MovieChangesModel
import com.eugeneosipenko.tmdb.db.MovieDAO
import com.eugeneosipenko.tmdb.db.MovieModel
import com.eugeneosipenko.tmdb.network.MovieService
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject


class MoviesViewModel @Inject constructor(
    private val service: MovieService,
    private val movieDAO: MovieDAO,
    private val movieChangesDAO: MovieChangesDAO
) : ViewModel() {

    private val invalidateSubject = PublishSubject.create<Boolean>()
    val movies: LiveData<PagedList<MovieModel>>

    init {

        val config = PagedList.Config.Builder()
            .setPageSize(20)
            .setInitialLoadSizeHint(40)
            .setEnablePlaceholders(false)
            .build()

        val factory = MovieDataSourceFactory(
            service, movieChangesDAO, movieDAO, invalidateSubject
        )

        movies = LivePagedListBuilder(factory, config).build()
    }

    fun setDateRange(range: Range): Completable {
        return service.getChanges(range.startDate)
            .distinct()
            .doOnNext { movieChangesDAO.clear() }
            .flatMap {
                Observable.fromArray(
                    it.results
                        .filter { !it.adult }
                        .map { MovieChangesModel(it.id, it.adult) }
                )
            }
            .flatMapCompletable {
                Completable.fromAction {
                    movieChangesDAO.insert(it)
                    invalidateSubject.onNext(true)
                }
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}