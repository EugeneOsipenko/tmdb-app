package com.eugeneosipenko.tmdb.data

import androidx.paging.DataSource
import com.eugeneosipenko.tmdb.db.MovieChangesDAO
import com.eugeneosipenko.tmdb.db.MovieDAO
import com.eugeneosipenko.tmdb.db.MovieModel
import com.eugeneosipenko.tmdb.network.MovieService
import io.reactivex.Observable


class MovieDataSourceFactory (
    private val service: MovieService,
    private val movieChangesDAO: MovieChangesDAO,
    private val movieDAO: MovieDAO,
    private val invalidateSubject: Observable<Boolean>
) : DataSource.Factory<Int, MovieModel>() {
    override fun create(): DataSource<Int, MovieModel> {
        return MovieDataSource(service, movieChangesDAO, movieDAO, invalidateSubject)
    }
}