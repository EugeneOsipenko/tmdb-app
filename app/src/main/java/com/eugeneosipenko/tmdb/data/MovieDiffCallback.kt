package com.eugeneosipenko.tmdb.data

import androidx.recyclerview.widget.DiffUtil
import com.eugeneosipenko.tmdb.db.MovieModel

class MovieDiffCallback : DiffUtil.ItemCallback<MovieModel>() {
    override fun areItemsTheSame(oldItem: MovieModel, newItem: MovieModel): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: MovieModel, newItem: MovieModel): Boolean {
        return oldItem == newItem
    }
}