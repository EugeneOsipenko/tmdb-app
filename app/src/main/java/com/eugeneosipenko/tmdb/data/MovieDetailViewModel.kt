package com.eugeneosipenko.tmdb.data

import androidx.lifecycle.ViewModel
import com.eugeneosipenko.tmdb.db.MovieDAO
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MovieDetailViewModel @Inject constructor(
    private val movieDAO: MovieDAO
): ViewModel() {

    fun movie(id: Int) =
            movieDAO.findById(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
}