package com.eugeneosipenko.tmdb.di

import android.content.Context
import androidx.room.Room
import com.eugeneosipenko.tmdb.db.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Singleton
    @Provides
    fun provideDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, "app-db").build()
    }

    @Singleton
    @Provides
    fun provideMovieDAO(db: AppDatabase) = db.movieDAO()

    @Singleton
    @Provides
    fun provideMovieChangesDAO(db: AppDatabase) = db.movieChangesDAO()
}