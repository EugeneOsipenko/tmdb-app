package com.eugeneosipenko.tmdb.di

import com.eugeneosipenko.tmdb.ui.MainActivity
import com.eugeneosipenko.tmdb.ui.MovieDetailsActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ViewModelModule::class, NetworkModule::class, AppModule::class, DatabaseModule::class])
interface CoreComponent {
    fun inject(activity: MainActivity)
    fun inject(activity: MovieDetailsActivity)
}