package com.eugeneosipenko.tmdb.network

import com.eugeneosipenko.tmdb.network.model.MovieChangesResponse
import com.eugeneosipenko.tmdb.network.model.MovieEntity
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieService {

    @GET("3/movie/changes")
    fun getChanges(
        @Query("start_date") startDate: String? = null
    ): Observable<MovieChangesResponse>

    @GET("3/movie/{movie_id}")
    fun getDetails(
        @Path("movie_id") id: Int
    ): Observable<MovieEntity>
}