package com.eugeneosipenko.tmdb.network.model

import se.ansman.kotshi.JsonSerializable

@JsonSerializable
data class MovieChangesEntity(
    val id: Int,
    val adult: Boolean = false
)