package com.eugeneosipenko.tmdb.network

import com.eugeneosipenko.tmdb.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response


class AuthInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val url = original.url().newBuilder()
                .addQueryParameter("api_key", BuildConfig.TMDB_API_KEY)
                .build()

        return chain.proceed(original.newBuilder().url(url).build())
    }
}