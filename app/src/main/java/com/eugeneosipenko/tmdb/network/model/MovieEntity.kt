package com.eugeneosipenko.tmdb.network.model

import com.squareup.moshi.Json
import se.ansman.kotshi.JsonSerializable

@JsonSerializable
data class MovieEntity(
    @Json(name = "id") val id: Int,
    @Json(name = "title") val title: String = "",
    @Json(name = "genres") val genres: List<GenreEntity>,
    @Json(name = "overview") val overview: String = "",
    @Json(name = "backdrop_path") val backdropPath: String = "",
    @Json(name = "poster_path") val posterPath: String = "",
    @Json(name = "release_date") val releaseDate: String = "",
    @Json(name = "vote_average") val voteAverage: Float
)