package com.eugeneosipenko.tmdb.network.model

import se.ansman.kotshi.JsonSerializable

@JsonSerializable
data class GenreEntity(
    val id: Int,
    val name: String
)