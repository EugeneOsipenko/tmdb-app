package com.eugeneosipenko.tmdb.network

import okhttp3.Interceptor
import okhttp3.Response



class RateLimitInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {

        var response = chain.proceed(chain.request())

        // 429 is how the api indicates a rate limit error
        if (!response.isSuccessful && response.code() == 429) {
            val cooldown = response.header("Retry-After")!!.toInt() * 1000 + 1000

            // wait & retry
            try {
                println("wait and retry...")
                Thread.sleep(cooldown.toLong())
            } catch (e: InterruptedException) {
                // no-op
            }

            response = chain.proceed(chain.request())
        }

        return response
    }
}