package com.eugeneosipenko.tmdb.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Maybe

@Dao
interface MovieDAO {

    @Query("SELECT * FROM MovieModel WHERE id = :id")
    fun findById(id: Int): Maybe<MovieModel>

    @Query("SELECT * FROM MovieModel")
    fun getAll(): Maybe<List<MovieModel>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(movie: MovieModel)
}