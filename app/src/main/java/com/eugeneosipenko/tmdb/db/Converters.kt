package com.eugeneosipenko.tmdb.db

import androidx.room.TypeConverter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types


class Converters {

    @TypeConverter
    fun fromString(value: String): List<String> {
        return adapter.fromJson(value) ?: arrayListOf()
    }

    @TypeConverter
    fun fromList(list: List<String>): String {
        return adapter.toJson(list)
    }

    companion object {
        private val moshi = Moshi.Builder().build()
        private val adapter = moshi.adapter<List<String>>(Types.newParameterizedType(List::class.java, String::class.java))
    }
}