package com.eugeneosipenko.tmdb.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MovieModel(
    @PrimaryKey
    val id: Int,
    val title: String,
    val genres: List<String>,
    val overview: String,
    val backdropPath: String = "",
    val posterPath: String = "",
    val releaseDate: String,
    val voteAverage: Float
)

