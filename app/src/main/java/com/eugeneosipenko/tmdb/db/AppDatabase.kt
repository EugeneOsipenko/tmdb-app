package com.eugeneosipenko.tmdb.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = [MovieModel::class, MovieChangesModel::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun movieDAO(): MovieDAO
    abstract fun movieChangesDAO(): MovieChangesDAO
}