package com.eugeneosipenko.tmdb.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Single

@Dao
interface MovieChangesDAO {

    @Query("SELECT * FROM MovieChangesModel LIMIT :count OFFSET :offset ")
    fun getChanges(offset: Int, count: Int): Single<List<MovieChangesModel>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(changes: List<MovieChangesModel>)

    @Query("DELETE FROM MovieChangesModel")
    fun clear()
}