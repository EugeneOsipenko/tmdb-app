package com.eugeneosipenko.tmdb.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MovieChangesModel(
    @PrimaryKey
    val id: Int,
    val adult: Boolean
)