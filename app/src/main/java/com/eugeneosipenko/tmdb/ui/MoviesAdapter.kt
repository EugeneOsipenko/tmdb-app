package com.eugeneosipenko.tmdb.ui

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.eugeneosipenko.tmdb.R
import com.eugeneosipenko.tmdb.db.MovieModel

class MoviesAdapter(
    diffCallback: DiffUtil.ItemCallback<MovieModel>,
    private val clickListener: (Int) -> Unit
) : PagedListAdapter<MovieModel, MoviesViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesViewHolder {
        return MoviesViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_movie,
                parent,
                false
            ), clickListener
        )
    }

    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int) {
        holder.movieId = getItem(position)?.id
        holder.title.text = getItem(position)?.title

        val background = getItem(position)?.posterPath ?: ""
        if (background.isEmpty()) {
            holder.image.setImageDrawable(ColorDrawable(Color.BLUE))
        } else {
            Glide.with(holder.itemView)
                .load("https://image.tmdb.org/t/p/w500$background")
                .transform(RoundedCorners(holder.image.resources.getDimensionPixelSize(R.dimen.default_corner_radius)))
                .into(holder.image)
        }
    }
}

class MoviesViewHolder(view: View, clickListener: (Int) -> Unit) : RecyclerView.ViewHolder(view) {
    val title: TextView = view.findViewById(R.id.title)
    val image: ImageView = view.findViewById(R.id.background)
    var movieId: Int? = null

    init {
        view.setOnClickListener { movieId?.let { clickListener.invoke(it) } }
    }
}