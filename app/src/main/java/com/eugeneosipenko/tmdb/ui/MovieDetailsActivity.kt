package com.eugeneosipenko.tmdb.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.eugeneosipenko.tmdb.data.MovieDetailViewModel
import com.eugeneosipenko.tmdb.db.MovieModel
import com.eugeneosipenko.tmdb.util.BlurTransformation
import com.eugeneosipenko.tmdb.util.component
import com.eugeneosipenko.tmdb.util.injectViewModel
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject




class MovieDetailsActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var detailsViewModel: MovieDetailViewModel

    private val disposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        component().inject(this)
        super.onCreate(savedInstanceState)
        setContentView(com.eugeneosipenko.tmdb.R.layout.activity_movie_details)

        window.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
            WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        )

        supportActionBar?.hide()
        detailsViewModel = injectViewModel(viewModelFactory)

        disposable.add(
            detailsViewModel
                .movie(intent.getIntExtra("ID", -1))
                .subscribe { initUI(it) })
    }

    private fun initUI(movie: MovieModel) {
        findViewById<TextView>(com.eugeneosipenko.tmdb.R.id.title).text = movie.title
        findViewById<TextView>(com.eugeneosipenko.tmdb.R.id.year).text = movie.releaseDate
        findViewById<TextView>(com.eugeneosipenko.tmdb.R.id.score_value).text = movie.voteAverage.toString()
        findViewById<TextView>(com.eugeneosipenko.tmdb.R.id.genres).text = movie.genres.joinToString { it }
        findViewById<TextView>(com.eugeneosipenko.tmdb.R.id.overview_value).text = movie.overview

        findViewById<ImageView>(com.eugeneosipenko.tmdb.R.id.poster).apply {
            Glide.with(this)
                .load("https://image.tmdb.org/t/p/w500${movie.posterPath}")
                .transform(RoundedCorners(resources.getDimensionPixelSize(com.eugeneosipenko.tmdb.R.dimen.default_corner_radius)))
                .into(this)
        }

        findViewById<ImageView>(com.eugeneosipenko.tmdb.R.id.backdrop).apply {
            Glide.with(this)
                .load("https://image.tmdb.org/t/p/w1280${movie.backdropPath}")
                .transform(CenterCrop(), BlurTransformation())
                .into(this)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }

    companion object {
        fun intent(context: Context, id: Int) =
            Intent(context, MovieDetailsActivity::class.java)
                .apply {
                    putExtra("ID", id)
                }
    }
}