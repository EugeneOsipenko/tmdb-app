package com.eugeneosipenko.tmdb.ui

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eugeneosipenko.tmdb.R
import com.eugeneosipenko.tmdb.data.MovieDiffCallback
import com.eugeneosipenko.tmdb.data.MoviesViewModel
import com.eugeneosipenko.tmdb.data.Range
import com.eugeneosipenko.tmdb.util.component
import com.eugeneosipenko.tmdb.util.injectViewModel
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var moviesViewModel: MoviesViewModel
    private lateinit var adapter: MoviesAdapter

    private var rangeDisposable: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        component().inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()
        moviesViewModel = injectViewModel(viewModelFactory)

        val spinner: Spinner = findViewById(R.id.spinner)
        val spinnerAdapter = ArrayAdapter.createFromResource(
            this,
            R.array.days,
            R.layout.item_spinner
        )

        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = spinnerAdapter
        spinner.setSelection(0)
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                // noop
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                changeRange(Range.forLastDays(position + 1))
            }
        }

        adapter = MoviesAdapter(MovieDiffCallback()) { showMovieDetails(it) }
        val moviesList = findViewById<RecyclerView>(R.id.movies_list)
        moviesList.layoutManager = GridLayoutManager(this, 3)
        moviesList.adapter = adapter

        changeRange(Range.today())
    }

    override fun onDestroy() {
        super.onDestroy()
        rangeDisposable?.dispose()
    }

    private fun changeRange(range: Range) {
        rangeDisposable?.dispose()
        rangeDisposable = moviesViewModel
            .setDateRange(range)
            .subscribe {
                moviesViewModel.movies.removeObservers(this)
                moviesViewModel.movies.observe(this, Observer { list -> adapter.submitList(list) })
            }
    }

    private fun showMovieDetails(id: Int) {
        startActivity(MovieDetailsActivity.intent(this, id))
    }
}
