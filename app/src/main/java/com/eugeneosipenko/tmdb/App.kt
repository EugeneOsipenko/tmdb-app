package com.eugeneosipenko.tmdb

import android.app.Application
import com.eugeneosipenko.tmdb.di.*

class App : Application() {

    lateinit var component: CoreComponent

    override fun onCreate() {
        super.onCreate()
        component = DaggerCoreComponent.builder()
            .networkModule(NetworkModule())
            .appModule(AppModule(this))
            .databaseModule(DatabaseModule())
            .build()
    }
}